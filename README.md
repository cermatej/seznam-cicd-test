# seznam-cicd-test

Minimal version of CI/CD pipeline that triggers onetime job in Databricks from template which in return writes into the original repository.

# Setup

### Gitlab prerequisites
* generate gitlab access token (api, read_api, read_repository, write_repository) and save to databricks scope `seznam-cicd` with key `gitlab-token`
### Databricks prerequisites
* generate Databricks token and save it in Gitlab project CI/CD variables as `BEARER_TOKEN` (`dapi...`)
* also save variable `DATABRICKS_ENDPOINT` with your Databricks instance URL. (`https://dbc-8c8038b8-0e28.cloud.databricks.com`)

#### Job setup
* Setup custom job in the UI (`branch` and `gitlab_project` job variables will be passed automatically)
* Copy the JSON specification but only `tasks` part (the rest will be added in the script) and save as a template
* Add CI/CD job to `.gitlab-ci.yml` with a call to a python script similar to this example (see GL CI/CD Documentation for advanced usage):
```
trigger_test:
  script:
    - >
      python3 trigger_test.py --input templates/simple_workflow.json --branch ${CI_COMMIT_BRANCH} --endpoint ${DATABRICKS_ENDPOINT} --git_url ${CI_PROJECT_URL}.git --token ${BEARER_TOKEN}
```