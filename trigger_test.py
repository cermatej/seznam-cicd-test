import argparse
import json
import requests
import os

# get arguments for run
parser = argparse.ArgumentParser()
parser.add_argument("--input", default='', type=str, help="Name of input JSON file.") # templates/simple_workflow.json
parser.add_argument("--branch", default='', type=str, help="Name of a branch on which the pipeline will be executed.") # main
parser.add_argument("--git_url", default='', type=str, help=".git url of the repository") # https://gitlab.com/cermatej/seznam-cicd-test
parser.add_argument("--endpoint", default='', type=str, help="databricks endpoint url") # https://dbc-8c8038b8-0e28.cloud.databricks.com
parser.add_argument("--token", default='', type=str, help="bearer token from secrets") # dapi...
args = parser.parse_args()

# load file
with open(args.input) as input_file:
  payload = json.loads(input_file.read())

# set parameters
payload["run_name"] = f"{os.path.basename(__file__).split('.')[0]}_pipeline"

# update git source
if 'git_source' not in payload:
    payload['git_source'] = {}
payload['git_source']['git_url'] = f"{args.git_url}.git"
payload['git_source']['git_branch'] = args.branch
payload['git_source']['git_provider'] = "gitLab"

# update if tasks are not set
for i, task in enumerate(payload['tasks']):
  if 'base_parameters' not in payload['tasks'][i]['notebook_task']:
    payload['tasks'][i]['notebook_task']['base_parameters'] = {}
  payload['tasks'][i]['notebook_task']['base_parameters'].update({
    'branch': args.branch,
    'gitlab_project': "/".join(args.git_url.replace('.git', '').split('/')[-2:]) # cermatej/seznam-cicd-test
  })

url = f"{args.endpoint}api/2.1/jobs/runs/submit"

headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization': f'Bearer {args.token}'
}

print(payload)
response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
print(response.text)
