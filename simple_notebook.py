# Databricks notebook source

# TODO: implement your deployment logic and then write the ID back to the original repository

# COMMAND ----------

import requests
import json
import urllib.parse
import random

# COMMAND ----------

branch = dbutils.widgets.get("branch") # main
gitlab_project = dbutils.widgets.get("gitlab_project") # 'cermatej/seznam-cicd-test'
gitlab_token = dbutils.secrets.get('seznam-cicd', 'gitlab-token')
FILENAME = 'stored_id.txt'
id = random.randint(0,10)

# COMMAND ----------

url = f"https://gitlab.com/api/v4/projects/{urllib.parse.quote_plus(gitlab_project)}/repository/files/{FILENAME}"

payload = json.dumps({
  "branch": branch,
  "author_email": "iamrobot@gitlab.com",
  "author_name": "Gitlab runner",
  "content": str(id),
  "commit_message": "ID updated"
})

headers = {
  'PRIVATE-TOKEN': gitlab_token,
  'Content-Type': 'application/json'
}

response = requests.request("PUT", url, headers=headers, data=payload)
print(response.text)
